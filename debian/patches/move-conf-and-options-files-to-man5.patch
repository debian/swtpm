Move *.conf and *.options files to man5
--- a/configure.ac
+++ b/configure.ac
@@ -595,6 +595,7 @@
 		src/utils/Makefile          \
 		man/Makefile                \
 		man/man3/Makefile           \
+		man/man5/Makefile           \
 		man/man8/Makefile           \
 		tests/Makefile              \
 		tests/test_config           \
--- a/man/Makefile.am
+++ b/man/Makefile.am
@@ -6,4 +6,5 @@
 
 SUBDIRS = \
 	man3 \
+	man5 \
 	man8
--- /dev/null
+++ b/man/man5/Makefile.am
@@ -0,0 +1,24 @@
+#
+# man/man5/Makefile.am
+#
+# For the license, see the COPYING file in the root directory.
+#
+
+
+man5_PODS = \
+	swtpm_setup.conf.pod \
+	swtpm-localca.options.pod \
+	swtpm-localca.conf.pod
+
+man5_MANS = \
+	swtpm_setup.conf.5 \
+	swtpm-localca.options.5 \
+	swtpm-localca.conf.5
+
+%.5 : %.pod
+	@pod2man -r "swtpm" \
+		-c "" \
+		-n $(basename $@) \
+		--section=5 $< > $@
+
+EXTRA_DIST = $(man5_MANS) $(man5_PODS)
--- a/man/man8/Makefile.am
+++ b/man/man8/Makefile.am
@@ -13,10 +13,7 @@
 	swtpm_ioctl.pod \
 	swtpm_localca.pod \
 	swtpm_setup.pod \
-	swtpm_setup.conf.pod \
-	swtpm-create-tpmca.pod \
-	swtpm-localca.options.pod \
-	swtpm-localca.conf.pod
+	swtpm-create-tpmca.pod
 
 man8_generated_MANS = \
 	swtpm.8 \
@@ -25,10 +22,7 @@
 	swtpm_ioctl.8 \
 	swtpm_localca.8 \
 	swtpm_setup.8 \
-	swtpm_setup.conf.8 \
-	swtpm-create-tpmca.8 \
-	swtpm-localca.options.8 \
-	swtpm-localca.conf.8
+	swtpm-create-tpmca.8
 
 if WITH_CUSE
 man8_generated_MANS += \
--- a/swtpm.spec.in
+++ b/swtpm.spec.in
@@ -155,12 +155,12 @@
 %{_mandir}/man8/swtpm_bios.8*
 %{_mandir}/man8/swtpm_cert.8*
 %{_mandir}/man8/swtpm_ioctl.8*
-%{_mandir}/man8/swtpm-localca.conf.8*
-%{_mandir}/man8/swtpm-localca.options.8*
+%{_mandir}/man5/swtpm-localca.conf.5*
+%{_mandir}/man5/swtpm-localca.options.5*
 %{_mandir}/man8/swtpm-localca.8*
 %{_mandir}/man8/swtpm_localca.8*
 %{_mandir}/man8/swtpm_setup.8*
-%{_mandir}/man8/swtpm_setup.conf.8*
+%{_mandir}/man5/swtpm_setup.conf.5*
 %config(noreplace) %{_sysconfdir}/swtpm_setup.conf
 %config(noreplace) %{_sysconfdir}/swtpm-localca.options
 %config(noreplace) %{_sysconfdir}/swtpm-localca.conf
--- /dev/null
+++ b/man/man5/swtpm-localca.conf.pod
@@ -0,0 +1,89 @@
+=head1 NAME
+
+swtpm-localca.conf - Configuration file for swtpm_localca
+
+=head1 DESCRIPTION
+
+The file I</etc/swtpm-localca.conf> contains configuration variables
+for the I<swtpm_localca> program.
+
+Entries may contain environment variables that will be resolved. All
+environment variables must be formatted like this: '${varname}'.
+
+Users may write their own configuration into
+I<${XDG_CONFIG_HOME}/swtpm-localca.conf> or if XDG_CONFIG_HOME
+is not set it may be in I<${HOME}/.config/swtpm-localca.conf>.
+
+The following configuration variables are supported:
+
+=over 4
+
+=item B<statedir>
+
+The name of a directory where to store data into. A lock will be created
+in this directory.
+
+=item B<signinkey>
+
+The file containing the key used for signing the certificates. Provide
+a key in PEM format or a pkcs11 URI.
+
+=item B<signingkey_password>
+
+The password to use for the signing key.
+
+=item B<issuercert>
+
+The file containing the certificate for this CA. Provide a certificate
+in PEM format.
+
+=item B<certserial>
+
+The name of file containing the serial number for the next certificate.
+
+=item B<TSS_TCSD_HOSTNAME>
+
+This variable can be set to the host where B<tcsd> is running on in case
+the signing key is a GnuTLS TPM 1.2 key. By default I<localhost> will be
+used.
+
+=item B<TSS_TCSD_PORT>
+
+This variable can be set to the port on which  B<tcsd> is listening for
+connections. By default port I<30003> will be used.
+
+=item B<env:<environment variable name>=<value>>
+
+Environment variables, that are needed by pkcs11 modules, can be set using
+this format. An example for such an environment variable may look like this:
+
+    env:MY_MODULE_PKCS11_CONFIG = /tmp/mymodule-pkcs11.conf
+
+The line must not contain any trailing spaces.
+
+=back
+
+=head1 EXAMPLE
+
+An example I<swtpm-localca.conf> file may look as follows:
+
+ statedir = /var/lib/swtpm_localca
+ signingkey = /var/lib/swtpm_localca/signkey.pem
+ issuercert = /var/lib/swtpm_localca/issuercert.pem
+ certserial = /var/lib/swtpm_localca/certserial
+
+With a PKCS11 URI it may look like this:
+
+ statedir = /var/lib/swtpm-localca
+ signingkey = pkcs11:model=SoftHSM%20v2;manufacturer=SoftHSM%20project;serial=891b99c169e41301;token=mylabel;id=%00;object=mykey;type=public
+ issuercert = /var/lib/swtpm-localca/swtpm-localca-tpmca-cert.pem
+ certserial = /var/lib/swtpm-localca/certserial
+ SWTPM_PKCS11_PIN = 1234
+
+=head1 SEE ALSO
+
+B<swtpm_localca>
+
+=head1 REPORTING BUGS
+
+Report bugs to Stefan Berger <stefanb@linux.vnet.ibm.com>
--- /dev/null
+++ b/man/man5/swtpm-localca.options.pod
@@ -0,0 +1,30 @@
+=head1 NAME
+
+swtpm-localca.options - Options file for swtpm_localca
+
+=head1 DESCRIPTION
+
+The file I</etc/swtpm-localca.options> contains command line options
+to pass to the I<swtpm_cert> program and should hold options
+that apply to the creation of all TPM Endorsement Key (EK) and platform
+certificates.
+
+Users may write their own options into
+I<${XDG_CONFIG_HOME}/swtpm-localca.options> or if XDG_CONFIG_HOME
+is not set it may be in I<${HOME}/.config/swtpm-localca.options>.
+
+=head1 EXAMPLE
+
+An example I<swtpm-localca.options> file may look as follows:
+
+  --platform-manufacturer Fedora
+  --platform-version 2.1
+  --platform-model QEMU
+
+=head1 SEE ALSO
+
+B<swtpm_localca>
+
+=head1 REPORTING BUGS
+
+Report bugs to Stefan Berger <stefanb@linux.vnet.ibm.com>
--- /dev/null
+++ b/man/man5/swtpm_setup.conf.pod
@@ -0,0 +1,106 @@
+=head1 NAME
+
+swtpm_setup.conf - Configuration file for swtpm_setup
+
+=head1 DESCRIPTION
+
+The file I</etc/swtpm_setup.conf> contains configuration information for
+swtpm_setup. It must only contain
+one configuration keyword per line, followed by an equals sign (=) and then
+followed by appropriate configuration information. A comment at the
+end of the line may be introduced by a hash (#) sign.
+
+Users may write their own configuration into
+I<${XDG_CONFIG_HOME}/swtpm_setup.conf> or if XDG_CONFIG_HOME
+is not set it may be in I<${HOME}/.config/swtpm_setup.conf>.
+
+The following keywords are recognized:
+
+=over 4
+
+=item B<create_certs_tool>
+
+This keyword is to be followed by the name of an executable or executable
+script used for creating various TPM certificates. The tool will be
+called with the following options
+
+=over 4
+
+=item B<--type type>
+
+This parameter indicates the type of certificate to create. The type parameter may
+be one of the following: I<ek>, or I<platform>
+
+=item B<--dir dir>
+
+This parameter indicates the directory into which the certificate is to be stored.
+It is expected that the EK certificate is stored in this directory under the name
+ek.cert and the platform certificate under the name platform.cert.
+
+=item B<--ek ek>
+
+This parameter indicates the modulus of the public key of the endorsement key
+(EK). The public key is provided as a sequence of ASCII hex digits.
+
+=item B<--vmid ID>
+
+This parameter indicates the ID of the VM for which to create the certificate.
+
+=item B<--logfile <logfile>>
+
+The log file to log output to; by default logging goes to stdout and stderr
+on the console.
+
+=item B<--configfile <configuration file>>
+
+The configuration file to use. This file typically contains configuration
+information for the invoked program. If omitted, the program must use
+its default configuration file.
+
+=item B<--optsfile <options file>>
+
+The options file to use. This file typically contains options that the
+invoked program uses. If omitted, the program must use its default
+options file.
+
+=item B<--tpm-spec-family <family>>, B<--tpm-spec-level <level>>, B<--tpm-spec-revision <revision>>
+
+These 3 options describe the TPM specification that was followed for
+the implementation of the TPM and will be part of the EK certificate.
+
+=item B<--tpm2>
+
+This option is passed in case a TPM 2 compliant certificate needs to be
+created.
+
+=back
+
+=item B<create_certs_tool_config>
+
+This keyword is to be followed by the name of a configuration file
+that will be passed to the invoked program using the --configfile
+option described above. If omitted, the invoked program will use
+the default configuration file.
+
+=item B<create_certs_tool_options>
+
+This keyword is to be followed by the name of an options file
+that will be passed to the invoked program using the --optsfile
+option described above. If omitted, the invoked program will use
+the default options file.
+
+=item B<active_pcr_banks> (since v0.7)
+
+This keyword is to be followed by a comma-separated list
+of names of PCR banks. The list must not contain any spaces.
+Valid PCR bank names are sha1, sha256, sha384, and sha512.
+
+=back
+
+=head1 SEE ALSO
+
+B<swtpm_setup>
+
+=head1 REPORTING BUGS
+
+Report bugs to Stefan Berger <stefanb@linux.vnet.ibm.com>
--- a/man/man8/swtpm-localca.conf.pod
+++ /dev/null
@@ -1,89 +0,0 @@
-=head1 NAME
-
-swtpm-localca.conf - Configuration file for swtpm_localca
-
-=head1 DESCRIPTION
-
-The file I</etc/swtpm-localca.conf> contains configuration variables
-for the I<swtpm_localca> program.
-
-Entries may contain environment variables that will be resolved. All
-environment variables must be formatted like this: '${varname}'.
-
-Users may write their own configuration into
-I<${XDG_CONFIG_HOME}/swtpm-localca.conf> or if XDG_CONFIG_HOME
-is not set it may be in I<${HOME}/.config/swtpm-localca.conf>.
-
-The following configuration variables are supported:
-
-=over 4
-
-=item B<statedir>
-
-The name of a directory where to store data into. A lock will be created
-in this directory.
-
-=item B<signinkey>
-
-The file containing the key used for signing the certificates. Provide
-a key in PEM format or a pkcs11 URI.
-
-=item B<signingkey_password>
-
-The password to use for the signing key.
-
-=item B<issuercert>
-
-The file containing the certificate for this CA. Provide a certificate
-in PEM format.
-
-=item B<certserial>
-
-The name of file containing the serial number for the next certificate.
-
-=item B<TSS_TCSD_HOSTNAME>
-
-This variable can be set to the host where B<tcsd> is running on in case
-the signing key is a GnuTLS TPM 1.2 key. By default I<localhost> will be
-used.
-
-=item B<TSS_TCSD_PORT>
-
-This variable can be set to the port on which  B<tcsd> is listening for
-connections. By default port I<30003> will be used.
-
-=item B<env:<environment variable name>=<value>>
-
-Environment variables, that are needed by pkcs11 modules, can be set using
-this format. An example for such an environment variable may look like this:
-
-    env:MY_MODULE_PKCS11_CONFIG = /tmp/mymodule-pkcs11.conf
-
-The line must not contain any trailing spaces.
-
-=back
-
-=head1 EXAMPLE
-
-An example I<swtpm-localca.conf> file may look as follows:
-
- statedir = /var/lib/swtpm_localca
- signingkey = /var/lib/swtpm_localca/signkey.pem
- issuercert = /var/lib/swtpm_localca/issuercert.pem
- certserial = /var/lib/swtpm_localca/certserial
-
-With a PKCS11 URI it may look like this:
-
- statedir = /var/lib/swtpm-localca
- signingkey = pkcs11:model=SoftHSM%20v2;manufacturer=SoftHSM%20project;serial=891b99c169e41301;token=mylabel;id=%00;object=mykey;type=public
- issuercert = /var/lib/swtpm-localca/swtpm-localca-tpmca-cert.pem
- certserial = /var/lib/swtpm-localca/certserial
- SWTPM_PKCS11_PIN = 1234
-
-=head1 SEE ALSO
-
-B<swtpm_localca>
-
-=head1 REPORTING BUGS
-
-Report bugs to Stefan Berger <stefanb@linux.vnet.ibm.com>
--- a/man/man8/swtpm-localca.options.pod
+++ /dev/null
@@ -1,30 +0,0 @@
-=head1 NAME
-
-swtpm-localca.options - Options file for swtpm_localca
-
-=head1 DESCRIPTION
-
-The file I</etc/swtpm-localca.options> contains command line options
-to pass to the I<swtpm_cert> program and should hold options
-that apply to the creation of all TPM Endorsement Key (EK) and platform
-certificates.
-
-Users may write their own options into
-I<${XDG_CONFIG_HOME}/swtpm-localca.options> or if XDG_CONFIG_HOME
-is not set it may be in I<${HOME}/.config/swtpm-localca.options>.
-
-=head1 EXAMPLE
-
-An example I<swtpm-localca.options> file may look as follows:
-
-  --platform-manufacturer Fedora
-  --platform-version 2.1
-  --platform-model QEMU
-
-=head1 SEE ALSO
-
-B<swtpm_localca>
-
-=head1 REPORTING BUGS
-
-Report bugs to Stefan Berger <stefanb@linux.vnet.ibm.com>
--- a/man/man8/swtpm_setup.conf.pod
+++ /dev/null
@@ -1,106 +0,0 @@
-=head1 NAME
-
-swtpm_setup.conf - Configuration file for swtpm_setup
-
-=head1 DESCRIPTION
-
-The file I</etc/swtpm_setup.conf> contains configuration information for
-swtpm_setup. It must only contain
-one configuration keyword per line, followed by an equals sign (=) and then
-followed by appropriate configuration information. A comment at the
-end of the line may be introduced by a hash (#) sign.
-
-Users may write their own configuration into
-I<${XDG_CONFIG_HOME}/swtpm_setup.conf> or if XDG_CONFIG_HOME
-is not set it may be in I<${HOME}/.config/swtpm_setup.conf>.
-
-The following keywords are recognized:
-
-=over 4
-
-=item B<create_certs_tool>
-
-This keyword is to be followed by the name of an executable or executable
-script used for creating various TPM certificates. The tool will be
-called with the following options
-
-=over 4
-
-=item B<--type type>
-
-This parameter indicates the type of certificate to create. The type parameter may
-be one of the following: I<ek>, or I<platform>
-
-=item B<--dir dir>
-
-This parameter indicates the directory into which the certificate is to be stored.
-It is expected that the EK certificate is stored in this directory under the name
-ek.cert and the platform certificate under the name platform.cert.
-
-=item B<--ek ek>
-
-This parameter indicates the modulus of the public key of the endorsement key
-(EK). The public key is provided as a sequence of ASCII hex digits.
-
-=item B<--vmid ID>
-
-This parameter indicates the ID of the VM for which to create the certificate.
-
-=item B<--logfile <logfile>>
-
-The log file to log output to; by default logging goes to stdout and stderr
-on the console.
-
-=item B<--configfile <configuration file>>
-
-The configuration file to use. This file typically contains configuration
-information for the invoked program. If omitted, the program must use
-its default configuration file.
-
-=item B<--optsfile <options file>>
-
-The options file to use. This file typically contains options that the
-invoked program uses. If omitted, the program must use its default
-options file.
-
-=item B<--tpm-spec-family <family>>, B<--tpm-spec-level <level>>, B<--tpm-spec-revision <revision>>
-
-These 3 options describe the TPM specification that was followed for
-the implementation of the TPM and will be part of the EK certificate.
-
-=item B<--tpm2>
-
-This option is passed in case a TPM 2 compliant certificate needs to be
-created.
-
-=back
-
-=item B<create_certs_tool_config>
-
-This keyword is to be followed by the name of a configuration file
-that will be passed to the invoked program using the --configfile
-option described above. If omitted, the invoked program will use
-the default configuration file.
-
-=item B<create_certs_tool_options>
-
-This keyword is to be followed by the name of an options file
-that will be passed to the invoked program using the --optsfile
-option described above. If omitted, the invoked program will use
-the default options file.
-
-=item B<active_pcr_banks> (since v0.7)
-
-This keyword is to be followed by a comma-separated list
-of names of PCR banks. The list must not contain any spaces.
-Valid PCR bank names are sha1, sha256, sha384, and sha512.
-
-=back
-
-=head1 SEE ALSO
-
-B<swtpm_setup>
-
-=head1 REPORTING BUGS
-
-Report bugs to Stefan Berger <stefanb@linux.vnet.ibm.com>
